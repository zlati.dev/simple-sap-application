## 1. Description
This is a simple application that fetches some data and after that transforms and works with the provided information.
The user experience with this app would be:

You'll see a list of items with some information like the name, short description, categories, price, and an "Add to Cart" button.
You can scroll down, and more items will load automatically.
You can use the "Sort" menu to sort the items by name or price.
<br>
## 2. Project view


![](./src/assets/v.png)


## 3. Setup Project view
To get the project up and running, follow these steps:

- Go inside the `src` folder.
- Run `npm install` to restore all dependencies.
- After that, the project can be run with `npm run dev`











# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh
