import { Button, } from "@chakra-ui/react";
import './styles/ButtonComponent.css';


function ButtonComponent({ children, onClick }) {


  return (
    <Button className='button'  onClick={onClick} >
      {children}
    </Button>

  )
}
export default ButtonComponent