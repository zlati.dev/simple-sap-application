import { useState, useEffect } from "react";
import "./App.css";
import {
  Stack,
  Card,
  CardBody,
  Text,
  Image,
  Box,
  Heading,
} from "@chakra-ui/react";
import ButtonComponent from "./ButtonComponent";
import InfiniteScroll from "react-infinite-scroll-component";
import axios from "axios";

import ParticlesBackground from "./ParticlesBackground";


import CategoriesMenu from "./CategoryMenu";

import SortMenu from "./SortMenu";

function App() {
  const [data, setData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [categories, setCategories] = useState([]);
  const categoriesMenu = new Set(categories);



  const handleAddToCart = () => {
    window.location.href = "https://greet.bg/?add-to-cart=5589.";
  };




  useEffect(() => {
    const api = async () => {
      const response = await axios(`/api?page=${pageNumber}`);
      setData(response.data);
      response.data.map((e) => {
        e.categories.map((checkTarget) => {
          categoriesMenu.add(checkTarget.name);
        });
        setCategories([...categoriesMenu]);
      });
      // console.log(response.data);
    };
    api();
  }, []);






  const reloadData = () => {
    const api = async () => {
      const response = await axios(`/api?page=${pageNumber + 1}`);
      response.data.map((e) => {
        e.categories.map((checkTarget) => {
          categoriesMenu.add(checkTarget.name);
        });
        setCategories([...categoriesMenu]);
      });
      setData(data.concat(response.data));
      // console.log(response.data);

      setPageNumber(pageNumber + 1);
    };
    api();
  };

  const sortByName = () => {
    let res = [...data]
      .map((e) => e)
      .sort((a, b) => a.name.localeCompare(b.name));

    setData(res);
  };
  const sortByPrice = () => {
    let res = [...data]
      .map((e) => e)
      .sort((a, b) => a.prices.price - b.prices.price);

    setData(res);
  };
  // console.log(categoriesMenu);



  const sortByCategory = (category) => {
    const condition = category.concat()
    const arr = []
    for (let index = 0; index < data.length; index++) { //C# training
      if (data[index].categories) {
        for (let w = 0; w < data[index].categories.length; w++) {
          if (data[index].categories[w].name === condition) {
            arr.push(data[index])
          }

        }
      }


    }
    setData(arr)

  }

const converter = (doc) => {
    const data = new DOMParser();
    const convertedData = data.parseFromString(doc, "text/html");
    return convertedData.body.textContent;
  };
  return (
    <>
      <ParticlesBackground />
      <Box className="select-box">
        <CategoriesMenu set={categoriesMenu} selectOption={sortByCategory} />
        <SortMenu one={sortByName} two={sortByPrice} />
      </Box>

      <InfiniteScroll
        dataLength={data.length}
        next={reloadData}
        hasMore={true}
        loader={<h3>Loading...</h3>}
        borderRadius={"16px"}
     endMessage={
      <Text>{"Няма намерени резултати"}</Text>
     }
      >
        <Card className="c-select">
          {data &&
            data.map((item) => (
              <CardBody className="custom-select" key={item.id}>
                <Image
                  src={item.images[0].src}
                  alt={item.name}
                  style={{
                    width: "216px",
                    height: "280px",
                    borderRadius: "13px",
                  }}
                  display={"flex"}
                />
                <Stack
                  marginInline="auto"
                  spacing={"1"}
                  width="80%"
                  maxHeight={"30%"}
                >
                  <Heading className="select-header">
                    {converter(item.name)}
                  </Heading>
                  <Text className="select-description">
                    {converter(
                      item.short_description
                        .replace("<p>", "")
                        .replace("</p>", "")
                    )}
                  </Text>
                  <Text color={"rgb(255, 255, 255)"} fontSize="2xl">
                    {item.categories.map((element) => (
                      <span key={element.id}>{element.name} /</span>
                    ))}
                  </Text>
                  <Box>
                    {item.prices.price} {item.prices.currency_code}
                  </Box>

                  <ButtonComponent display={"flex"} onClick={handleAddToCart}>
                    {item.add_to_cart.text}
                  </ButtonComponent>
                </Stack>
              </CardBody>
            ))}
        </Card>
      </InfiniteScroll>
    </>
  );
}

export default App;
