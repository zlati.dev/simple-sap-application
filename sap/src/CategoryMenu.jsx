import "./styles/CategoryMenu.css";

function CategoriesMenu({ set, selectOption }) {
  const byCategory = (e) => {
    selectOption(e.target.value);
  };

  return (
    <div>
      <select className="custom-select-category" onChange={byCategory}>
        {[...set].map((item, index) => {
          return (
            <option key={index} value={item}>
              {item}
            </option>
          );
        })}
      </select>
    </div>
  );
}
export default CategoriesMenu;
