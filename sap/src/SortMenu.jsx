import { useState } from "react";

import './styles/SortMenu.css';

function SortMenu({ one, two }) {
  const [select, setSelector] = useState("");

  const hanSort = (e) => {
    setSelector(e.target.value);
    switch (e.target.value) {
      case "1":
        one();
        break;
      case "2":
        two();
        break;
    }
  };

  return (
    <div className="custom-menu" >
      <select value={select} onChange={hanSort}>
        <option>{"Сортиране"}</option>
        <option value="1">{"Име"}</option>
        <option value="2">{"Цена"}</option>
      </select>
    </div>
  );
}
export default SortMenu;
