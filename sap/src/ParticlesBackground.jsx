import Particles from "react-particles";
import { loadFull } from "tsparticles";
import { particles } from '../particles-config';

function ParticlesBackground() {
  const particlesInit = async (main) => {
    await loadFull(main);
  };

  return (
    <Particles id="tsparticles" init={particlesInit} options={particles} />
  );
}


export default ParticlesBackground
